/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.protection;

/**
 *
 * @author Dan
 */
public enum ProtectionFlag {
    
    Build("build","Build", false),
    CreeperExplode("creeper-explode","Creeper Explode", false),
    IceMelt("ice-melt","Ice Melting", true),
    ;
    
    public String wgIdentifier;
    public String name;
    public Object value;
    
    ProtectionFlag(String identifier, String name, boolean defaultValue) {
        this.wgIdentifier = identifier;
        this.name = name;
        this.value = defaultValue;
    }
    
    public String getName() {
        return name;
    }
    
    public String getIdentifier() {
        return wgIdentifier;
    }
    
    public Object getValue() {
        return value;
    }

    public String getKey() {
        return wgIdentifier;
    }
}
