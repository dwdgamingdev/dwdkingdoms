/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.objects;

import net.downwithdestruction.dwdkingdoms.DwDKingdomsPlugin;
import org.bukkit.entity.Player;

/**
 *
 * @author Dan
 */
public class KingdomMember extends RegionMember {

    private KingdomRank rank = getRegion().getKingdom().getDefaultRank();
    private Kingdom kingdom;

    public KingdomRank getRank() {
        return rank;

    }

    public Kingdom getKingdom() {

        return kingdom;
    }

    public void updatePermissions() {
        Player p = getDwDPlayer().getPlayer();
        String node = getRank().getPermissionNode();
        String world = getKingdom().getWorld().getName();
        if (!p.hasPermission(node)) {
            DwDKingdomsPlugin.getPlugin().addPermission(p.getName(),node,world);
        }
    }
}
