/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.objects;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.downwithdestruction.dwdcore.players.DwDPlayer;
import net.downwithdestruction.dwdkingdoms.DwDKingdomsPlugin;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author Dan
 */
public class Kingdom {

    private World world;
    private String name;
    private DwDPlayer owner;
    private DwDPlayer coowner;
    private KingdomRank defaultRank = null;
    private Map<String, KingdomRank> ranks = new HashMap<>();
    private Region region;
    private Map<Integer, Plot> plots = new HashMap<>();

    public Kingdom(String kingdomName, World world) {
        this.name = kingdomName;
        this.world = world;

    }
    
    public Kingdom(World world) {
        this.world = world;
    }

    public void addRank(KingdomRank rank) {
        if (!ranks.containsKey(rank.getName())) {
            ranks.put(rank.getName(), rank);
        }
    }

    public void removeRank(KingdomRank rank) {
        if (ranks.containsKey(rank.getName())) {
            ranks.remove(rank.getName());
        }
    }

    public void addPlot(Plot plot) {
        if (!hasPlot(plot.getPlotID())) {
            plots.put(plot.getPlotID(), plot);
        }
    }

    public boolean hasPlot(int plotID) {
        if(plots.containsKey(plotID))
            return true;
        return false;
    }

    public Plot getPlot(int plotID) {
        if(hasPlot(plotID))
            return plots.get(plotID);
        return null;
    }

    public Plot getPlot(Location location) {
        return null;
    }

    public Collection<Plot> getPlots() {
        return plots.values();
    }

    public KingdomRank getDefaultRank() {
        return defaultRank;
    }

    public World getWorld() {
        return world;
    }

    public String getName() {
        return name;
    }

    public DwDPlayer getOwner() {
        return owner;
    }

    public DwDPlayer getCoOwner() {
        return coowner;
    }

    public Collection<KingdomRank> getRanks() {
        return ranks.values();
    }

    public Collection<KingdomMember> getMembers() {
        return region.getKingdomMembers();
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }
    
    public void setOwner(DwDPlayer owner) {
        if(getOwner() != null)
            DwDKingdomsPlugin.getPlugin().removePermission(getOwner().getName(),"DwD.L",getWorld().getName());
        this.owner = owner;
        DwDKingdomsPlugin.getPlugin().addPermission(getOwner().getName(),"DwD.L",getWorld().getName());
        updateMembers();
    }
    
    public void setCoOwner(DwDPlayer coowner) {
        if(getCoOwner() != null)
            DwDKingdomsPlugin.getPlugin().removePermission(getCoOwner().getName(),"DwD.HC",getWorld().getName());
        this.coowner = coowner;
        DwDKingdomsPlugin.getPlugin().addPermission(getCoOwner().getName(),"DwD.HC",getWorld().getName());
        updateMembers();
    }
    
    private void updateMembers() {
        Set<DwDPlayer> exMembers = new HashSet<>();
        if(getOwner() != null)
            exMembers.add(owner);
        if(getCoOwner() != null)
            exMembers.add(coowner);
        getRegion().setExMembers(exMembers);
    }
    
    public void setDefaultRank(KingdomRank rank) {
        this.defaultRank = rank;
    }
    
    public void setWorld(World world) {
        this.world = world;
    }
    
    public boolean playerIsMember(DwDPlayer player) {
        if(getOwner() != null && getOwner().equals(player))
            return true;
        if(getCoOwner() != null && getOwner().equals(player))
            return true;
        for(KingdomMember kingdomMember : getMembers()) {
            if(kingdomMember.getDwDPlayer().equals(player))
                return true;
        }
        return false;
    }

    public void appendData(ConfigurationSection confSection) {
        confSection.set("general.name", getName());
        if(getOwner() != null)
            confSection.set("general.owner", getOwner().getName());
        if(getCoOwner() != null)
            confSection.set("general.coowner", getCoOwner().getName());
        if(getDefaultRank() != null)
            confSection.set("general.defaultRank", getDefaultRank().getName());

        int x = 0;
        for (KingdomRank kingdomRank : getRanks()) {
            confSection.set("ranks." + x, kingdomRank.getName());
            x++;
        }
        
        ConfigurationSection plotsSection = confSection.createSection("plots");
        for(Plot plot : getPlots()) {
            plot.appendData(plotsSection);
        }

        getRegion().appendData(confSection);
    }
    
    public void loadData(ConfigurationSection confSection) {
        setName(confSection.getString("general.name"));
        Region r = new Region(getWorld());
        r.loadData(confSection);
        setRegion(r);
        if(confSection.getString("general.owner") != null)
            setOwner(new DwDPlayer(confSection.getString("general.owner")));
        if(confSection.getString("general.coowner") != null)
            setCoOwner(new DwDPlayer(confSection.getString("general.coowner")));
        if(confSection.getString("general.defaultRank") != null)
            setDefaultRank(RankManager.getRank(confSection.getString("general.defaultRank")));
        
        ConfigurationSection plotsSection = confSection.getConfigurationSection("plots");
        if(plotsSection != null) {
            Set<String> keys = plotsSection.getKeys(false);
            for(String plotID : keys) {
                ConfigurationSection plotIDSection = plotsSection.getConfigurationSection(plotID);
                Plot plot = new Plot(Integer.parseInt(plotID), this);
                plot.loadData(plotIDSection);
                addPlot(plot);
            }
        }
    }
}
