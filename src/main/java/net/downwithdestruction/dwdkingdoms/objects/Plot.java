/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.objects;

import com.sk89q.worldedit.bukkit.selections.Selection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import net.downwithdestruction.dwdcore.players.DwDPlayer;
import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author Dan
 */
public class Plot {

    private int plotID;
    private String plotName;
    private DwDPlayer plotOwner;
    private Kingdom kingdom;
    private Region region;
    private double price = 0.00;
    
    private boolean saleOffered = false;
    private Date saleOfferedTime;
    
    public Plot(int plotID, Kingdom kingdom, Selection selection) {
        this.plotID = plotID;
        this.kingdom = kingdom;
        this.region = new Region(this,selection.getMinimumPoint(),selection.getMaximumPoint());
    }
    
    public Plot(int plotID, Kingdom kingdom) {
        this.plotID = plotID;
        this.kingdom = kingdom;
    }

    public int getPlotID() {
        return plotID;
    }

    public DwDPlayer getOwner() {
        return plotOwner;
    }

    public Kingdom getKingdom() {
        return kingdom;
    }

    public Region getRegion() {
        return region;
    }

    public String getPlotName() {
        return plotName;
    }

    public void setPlotID(int plotID) {
        this.plotID = plotID;
    }

    public void setPlotName(String name) {
        this.plotName = name;
    }

    public void setOwner(DwDPlayer owner) {
        this.plotOwner = owner;
        updateMembers();
    }
    
    public void setRegion(Region region) {
        this.region = region;
        updateMembers();
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public double getPrice() {
        return price;
    }
    
    public void initSell() {
        this.saleOffered = true;
        this.saleOfferedTime = new Date();
    }
    
    private void updateMembers() {
        Set<DwDPlayer> exMembers = new HashSet<>();
        exMembers.add(getOwner());
        getRegion().setExMembers(exMembers);
    }
    
    public void appendData(ConfigurationSection confSection) {
        ConfigurationSection section = confSection.createSection(""+getPlotID());
        if(getPlotName() != null)
            section.set("plotName", getPlotName());
        if(getOwner() != null)
            section.set("plotOwner", getOwner().getName());
        section.set("plotPrice", getPrice());
        
        getRegion().appendData(section);
    }
    
    public void loadData(ConfigurationSection confSection) {
        Region r = new Region(getKingdom().getWorld());
        r.loadData(confSection);
        setRegion(r);
        if(confSection.getString("plotName") != null)
            setPlotName(confSection.getString("plotName"));
        if(confSection.getString("plotOwner") != null)
            setOwner(new DwDPlayer(confSection.getString("plotOwner")));
        setPrice(confSection.getDouble("plotPrice"));
        
    }
}
