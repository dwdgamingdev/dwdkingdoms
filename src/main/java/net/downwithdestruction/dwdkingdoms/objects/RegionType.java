/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.objects;

/**
 *
 * @author Dan
 */
public enum RegionType {
    Residential("residential"),
    Community("community"),
    Kingdom("kingdom");

    private String identifier;
    
    private RegionType(String identifier) {
        this.identifier = identifier;
    }
    
    public String getIdentifier() {
        return identifier;
    }
    
    public static RegionType getByIdentifier(String identifier) {
        for(RegionType regionType : values()) {
            if(regionType.getIdentifier().equalsIgnoreCase(identifier))
                return regionType;
        }
        return RegionType.Residential;
    }
    
}
