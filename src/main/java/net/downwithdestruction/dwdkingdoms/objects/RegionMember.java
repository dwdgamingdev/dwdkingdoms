/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.objects;

import net.downwithdestruction.dwdcore.players.DwDPlayer;

/**
 *
 * @author Dan
 */
public class RegionMember {
    
    public DwDPlayer player;
    public Region region;
    
    public DwDPlayer getDwDPlayer() {
        return player;
    }
    
    public Region getRegion() {
        return region;
    }
}
