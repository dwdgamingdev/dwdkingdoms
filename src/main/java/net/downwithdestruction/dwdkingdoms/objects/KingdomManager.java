/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.objects;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.downwithdestruction.dwdcore.players.DwDPlayer;
import net.downwithdestruction.dwdkingdoms.DwDKingdomsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Dan
 */
public class KingdomManager {

    public Map<String, Kingdom> kingdoms = new HashMap<>();

    public KingdomManager() {
    }

    public Kingdom getKingdom(String name) {
        if (kingdoms.containsKey(name)) {
            return kingdoms.get(name);
        }
        return null;
    }

    public Kingdom getKingdom(World world) {
        for (Kingdom kingdom : kingdoms.values()) {
            if (kingdom.getWorld().equals(world)) {
                return kingdom;
            }
        }
        return null;
    }

    public Collection<Kingdom> getKingdoms() {
        return kingdoms.values();
    }

    public void createKingdom(String kingdomName, String world) {
        World Bworld = Bukkit.getWorld(world);
        if (Bworld != null) {
            createKingdom(kingdomName, Bworld);
        }
    }

    public void createKingdom(String kingdomName, World world) {
        Kingdom kingdom = new Kingdom(kingdomName, world);
        Region region = new Region(kingdom);
        kingdom.setRegion(region);
        kingdoms.put(kingdomName, kingdom);
    }

    public void deleteKingdom(String kingdomName) {
        if (kingdoms.containsKey(kingdomName)) {
            kingdoms.remove(kingdomName);
        }
    }

    public void deleteKingdom(World world) {
        for (Kingdom kingdom : kingdoms.values()) {
            if (kingdom.getWorld().equals(world)) {
                kingdoms.remove(kingdom.getName());
            }
        }
    }

    public void deleteKingdom(Kingdom kingdomArg) {
        for (Kingdom kingdom : kingdoms.values()) {
            if (kingdom.equals(kingdomArg)) {
                kingdoms.remove(kingdom.getName());
            }
        }
    }
    
    public boolean playerIsMember(DwDPlayer player) {
        for(Kingdom kingdom : kingdoms.values()) {
            if(kingdom.playerIsMember(player))
                return true;
        }
        return false;
    }

    public void saveData() {
        File file;
        for (Kingdom kingdom : kingdoms.values()) {
            try {
                String worldName = kingdom.getWorld().getName();
                file = new File(DwDKingdomsPlugin.getPlugin().getDataFolder().toString() + File.separatorChar + "kingdoms" + File.separatorChar + worldName + ".yml");

                YamlConfiguration ymlFile = new YamlConfiguration();

                kingdom.appendData(ymlFile.createSection("kingdom"));

                ymlFile.save(file);
            } catch (IOException ex) {
                Logger.getLogger(KingdomManager.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @SuppressWarnings("unchecked")
    public void loadData() {
        File[] files = new File(DwDKingdomsPlugin.getPlugin().getDataFolder().toString() + File.separatorChar + "kingdoms").listFiles();
        for (File file : files) {
            String worldName = file.getName();
            int pos = worldName.lastIndexOf('.');
            String ext = worldName.substring(pos + 1);
            if (ext.equalsIgnoreCase("yml")) {
                worldName = worldName.replaceAll(".yml", "");
                World world = Bukkit.getWorld(worldName);
                if (world != null) {
                    YamlConfiguration ymlFile = YamlConfiguration.loadConfiguration(file);
                    
                    Kingdom kingdom = new Kingdom(world);
                    kingdom.setWorld(world);
                    kingdom.loadData(ymlFile.getConfigurationSection("kingdom"));
                    kingdoms.put(kingdom.getName(), kingdom);
                } else {
                }
            }
        }
    }
}
