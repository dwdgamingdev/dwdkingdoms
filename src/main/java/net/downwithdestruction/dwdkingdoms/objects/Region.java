/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.objects;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.databases.ProtectionDatabaseException;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.GlobalProtectedRegion;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.downwithdestruction.dwdcore.players.DwDPlayer;
import net.downwithdestruction.dwdkingdoms.protection.ProtectionFlag;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author Dan
 */
public class Region {

    private static final long serialVersionUID = 1L;
    private String regionName = "";
    private RegionType regionType = RegionType.Residential;
    private Set<ProtectionFlag> protectionFlags = new HashSet<>();
    private Set<RegionMember> members = new HashSet<>();
    private Set<DwDPlayer> exMembers = new HashSet<>();
    private ProtectedRegion worldguardRegion = null;
    private WorldGuardPlugin wgPlugin = (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin("WorldGuard");
    private RegionManager regionManager;

    public Region(Plot plot, Location minLocation, Location maxLocation) {
        setup(plot.getKingdom().getWorld());
        this.regionName = plot.getKingdom().getName().toLowerCase().replaceAll(" ", "_") + "_plot" + plot.getPlotID();

        BlockVector minLoc = new BlockVector(minLocation.getBlockX(), minLocation.getBlockY(), minLocation.getBlockZ());
        BlockVector maxLoc = new BlockVector(maxLocation.getBlockX(), maxLocation.getBlockY(), maxLocation.getBlockZ());

        ProtectedRegion region = new ProtectedCuboidRegion(regionName, minLoc, maxLoc);
        if (regionManager.hasRegion(regionName)) {
            regionManager.removeRegion(regionName);
        }
        regionManager.addRegion(region);

        try {
            region.setParent(plot.getKingdom().getRegion().getWGRegion());
        } catch (ProtectedRegion.CircularInheritanceException e) {
            e.printStackTrace();
        }

        try {
            regionManager.save();
        } catch (ProtectionDatabaseException e) {
            e.printStackTrace();
        }
        setWGRegion(region);
    }

    public Region(Kingdom kingdom) {
        setup(kingdom.getWorld());
        this.regionName = kingdom.getName().toLowerCase().replaceAll(" ", "_") + "_kingdom";
        this.regionType = RegionType.Kingdom;

        ProtectedRegion region = new GlobalProtectedRegion(regionName);
        if (regionManager.hasRegion(regionName)) {
            regionManager.removeRegion(regionName);
        }
        worldguardRegion = region;
        regionManager.addRegion(region);
        try {
            regionManager.save();
        } catch (ProtectionDatabaseException e) {
            e.printStackTrace();
        }
        setWGRegion(region);
    }

    public Region(World world) {
        setup(world);
    }

    private void setup(World world) {
        this.regionManager = wgPlugin.getRegionManager(world);
    }

    public String getID() {
        return worldguardRegion.getId();
    }

    private void setWGRegion(ProtectedRegion region) {
        this.worldguardRegion = region;
    }

    public Set<KingdomMember> getKingdomMembers() {
        Set<KingdomMember> collection = new HashSet<>();

        for (RegionMember member : members) {
            if (member instanceof KingdomMember) {
                collection.add((KingdomMember) member);
            }
        }

        return collection;
    }

    public Set<PlotMember> getPlotMembers() {
        Set<PlotMember> collection = new HashSet<>();

        for (RegionMember member : members) {
            if (member instanceof PlotMember) {
                collection.add((PlotMember) member);
            }
        }

        return collection;
    }

    public void updateFlags() {
        Map<Flag<?>, Object> flags = new HashMap<>();

        try {
            for (ProtectionFlag protFlag : protectionFlags) {
                System.out.println("Key = " + protFlag.getKey() + ", Value = " + protFlag.getValue());
                
                Flag<?> foundFlag = null;

                for (Flag<?> flag : DefaultFlag.getFlags()) {
                    if (flag.getName().replace("-", "").equalsIgnoreCase(protFlag.getIdentifier().replace("-", ""))) {
                        foundFlag = flag;
                        break;
                    }
                }

                if (foundFlag == null) {
                    break;
                }

                flags.put(foundFlag, protFlag.getValue());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        worldguardRegion.setFlags(flags);
    }

    public void updateMembers() {
        DefaultDomain wgMembers = new DefaultDomain();
        for (RegionMember member : members) {
            if (member instanceof KingdomMember) {
                KingdomMember kingdomMember = (KingdomMember) member;
                if (kingdomMember.getRank().canBuild()) {
                    wgMembers.addPlayer(member.getDwDPlayer().getName());
                }
            } else {
                wgMembers.addPlayer(member.getDwDPlayer().getName());
            }
        }

        for (DwDPlayer player : exMembers) {
            wgMembers.addPlayer(player.getName());
        }

        worldguardRegion.setMembers(wgMembers);
    }

    public Kingdom getKingdom() {
        return null;
    }

    public Plot getPlot() {
        return null;
    }

    public int getMinX() {
        return worldguardRegion.getMinimumPoint().getBlockX();
    }

    public int getMinY() {
        return worldguardRegion.getMinimumPoint().getBlockY();
    }

    public int getMinZ() {
        return worldguardRegion.getMinimumPoint().getBlockZ();
    }

    public int getMaxX() {
        return worldguardRegion.getMaximumPoint().getBlockX();
    }

    public int getMaxY() {
        return worldguardRegion.getMaximumPoint().getBlockY();
    }

    public int getMaxZ() {
        return worldguardRegion.getMaximumPoint().getBlockZ();
    }

    private ProtectedRegion getWGRegion() {
        return worldguardRegion;
    }

    public RegionType getRegionType() {
        return regionType;
    }
    
    public void setExMembers(Set<DwDPlayer> exMembers) {
        this.exMembers = exMembers;
        updateMembers();
    }

    public void appendData(ConfigurationSection confSection) {
        confSection.set("region.id", getID());
        confSection.set("region.type", getRegionType().getIdentifier());
        confSection.set("region.min.x", getMinX());
        confSection.set("region.min.y", getMinX());
        confSection.set("region.min.z", getMinX());
        confSection.set("region.max.x", getMaxX());
        confSection.set("region.max.y", getMaxY());
        confSection.set("region.max.z", getMaxZ());

        for (ProtectionFlag flag : protectionFlags) {
            confSection.set("region.flags." + flag.getIdentifier(), flag.getValue());
        }

        /*for(RegionMember regionMember : members) {
         String memberName = regionMember.getDwDPlayer().getName();
         confSection.set("region.members."+memberName+".rank", regionMember.getRank())
         }*/
    }

    public void loadData(ConfigurationSection confSection) {
        this.worldguardRegion = regionManager.getRegion(confSection.getString("region.id"));
        this.regionType = RegionType.getByIdentifier(confSection.getString("region.type"));
        updateFlags();
        updateMembers();
    }
}
