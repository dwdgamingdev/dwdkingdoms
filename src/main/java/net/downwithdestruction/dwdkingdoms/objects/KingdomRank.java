/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.objects;

/**
 *
 * @author Dan
 */
public class KingdomRank {
    
    private String name;
    private String permissionNode;
    private boolean canBuild = false;
    
    public KingdomRank(String name) {
        this.name = name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setPermissionNode(String node) {
        this.permissionNode = node;
    }
    
    public String getPermissionNode() {
        return permissionNode;
    }
    
    public void canBuild(boolean build) {
        this.canBuild = build;
    }
    
    public boolean canBuild() {
        return canBuild;
    }
    
}
