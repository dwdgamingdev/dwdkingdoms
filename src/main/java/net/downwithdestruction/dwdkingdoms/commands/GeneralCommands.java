/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.commands;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.minecraft.util.commands.NestedCommand;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.downwithdestruction.dwdkingdoms.DwDKingdomsPlugin;
import net.downwithdestruction.dwdkingdoms.objects.Kingdom;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Dan
 */
public class GeneralCommands {

    private static DwDKingdomsPlugin plugin = DwDKingdomsPlugin.getPlugin();

    @Command(aliases = {"dwdkingdoms"}, usage = "", flags = "", desc = "Kingdom commands.", help = "All commands related to Kingdoms.", min = 0, max = 0)
    @NestedCommand(value = {CoreCommands.class})
    public static void dwdkingdoms(CommandContext args, CommandSender sender) throws CommandException {
    }

    @Command(aliases = {"kingdom"}, usage = "", flags = "", desc = "Kingdom commands.", help = "All commands related to Kingdoms.", min = 0, max = 0)
    @NestedCommand(value = {KingdomCommands.class})
    public static void kingdom(CommandContext args, CommandSender sender) throws CommandException {
    }

    @Command(aliases = {"plot"}, usage = "", flags = "", desc = "Plot commands.", help = "All commands related to Plots.", min = 0, max = 0)
    @NestedCommand(value = {PlotCommands.class})
    public static void plot(CommandContext args, CommandSender sender) throws CommandException {
    }

    @Command(aliases = {"kingdoms", "kdlist"}, usage = "", flags = "", desc = "List all current kingdoms", help = "", min = 0, max = 0)
    @CommandPermissions("dwdkingdoms.commands.kingdoms")
    @Console
    public static void kingdoms(CommandContext args, CommandSender sender) throws CommandException {
        String buffer = plugin.getLanguage().get("commands.kingdoms.prefix");
        Iterator<Kingdom> kingdoms = plugin.getKingdomManager().getKingdoms().iterator();
        int x = 0;
        
        while (kingdoms.hasNext()) {
            Kingdom kingdom = kingdoms.next();
            Map<String, Object> item = new HashMap<>();
            item.put("%N", kingdom.getName());
            item.put("%W", kingdom.getWorld().getName());
            String owner = "Nobody";
            if(kingdom.getOwner() != null) {
                owner = kingdom.getOwner().getName();
            }
            item.put("%O", owner);
            buffer += plugin.getLanguage().get("commands.kingdoms.item", item);
            if(kingdoms.hasNext())
                buffer += plugin.getLanguage().get("commands.kingdoms.seperator");
        }
        buffer += plugin.getLanguage().get("commands.kingdoms.suffix");
        sender.sendMessage(buffer);
    }
}
