/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.commands;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.minecraft.util.commands.NestedCommand;
import java.util.HashMap;
import java.util.Map;
import net.downwithdestruction.dwdcore.players.DwDPlayer;
import net.downwithdestruction.dwdkingdoms.DwDKingdomsPlugin;
import net.downwithdestruction.dwdkingdoms.objects.Kingdom;
import net.downwithdestruction.dwdkingdoms.objects.KingdomMember;
import net.downwithdestruction.dwdkingdoms.objects.KingdomRank;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Dan
 */
public class KingdomCommands {

    private static DwDKingdomsPlugin plugin = DwDKingdomsPlugin.getPlugin();

    @Command(aliases = {"create", "c"}, usage = "", flags = "", desc = "Create a new kingdom.", help = "[world]", min = 1, max = 2)
    @CommandPermissions("dwdkingdoms.commands.kingdom.create")
    @Console
    public static void create(CommandContext args, CommandSender sender) throws CommandException {
        World world = null;
        if (args.argsLength() == 2) {
            String worldname = args.getString(1);
            world = Bukkit.getWorld(worldname);
            if (world == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", worldname);
                sender.sendMessage(plugin.getLanguage().get("error.invalidWorld", vars));
                return;
            }
        } else if (sender instanceof Player) {
            Player player = (Player) sender;
            world = player.getWorld();
        } else {
            sender.sendMessage(plugin.getLanguage().get("commands.kingdom.create.help"));
            return;
        }

        if (plugin.getKingdomManager().getKingdom(args.getString(0)) != null) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%A", args.getString(0));
            sender.sendMessage(plugin.getLanguage().get("error.kingdomAlreadyExists", vars));
            return;
        }

        if (plugin.getKingdomManager().getKingdom(world) != null) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%A", args.getString(0));
            vars.put("%W", world.getName());
            sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldAlreadyExists", vars));
            return;
        }

        plugin.getKingdomManager().createKingdom(args.getString(0), world);
        Map<String, Object> vars = new HashMap<>();
        vars.put("%W", world.getName());
        vars.put("%N", args.getString(0));
        sender.sendMessage(plugin.getLanguage().get("commands.kingdom.create.created", vars));
    }

    @Command(aliases = {"info", "i"}, usage = "", flags = "", desc = "Display information on a Kingdom", help = "[kingdom]", min = 0, max = 1)
    @CommandPermissions("dwdkingdoms.commands.kingdom.info")
    @Console
    public static void info(CommandContext args, CommandSender sender) throws CommandException {
        Kingdom kingdom = null;
        if (args.argsLength() == 1) {
            kingdom = plugin.getKingdomManager().getKingdom(args.getString(0));
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else if (sender instanceof Player) {
            Player player = (Player) sender;
            World world = player.getWorld();
            kingdom = plugin.getKingdomManager().getKingdom(world);
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%W", world.getName());
                sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldDoesntExist", vars));
                return;
            }
        } else {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%A", args.getString(0));
            sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
            return;
        }


        Map<String, Object> vars = new HashMap<>();
        String rankList = "";
        String memberList = "";
        boolean first = true;

        for (KingdomRank rank : kingdom.getRanks()) {
            if (first == false) {
                rankList += plugin.getLanguage().get("commands.kingdom.info.rank.separator");
            }
            first = false;
            Map<String, Object> rankVars = new HashMap<>();
            rankVars.put("%N", rank.getName());
            rankVars.put("%P", rank.getPermissionNode());
            rankVars.put("%C", (rank.canBuild() ? plugin.getLanguage().get("commands.kingdom.info.rank.canBuildTrue") : plugin.getLanguage().get("commands.kingdom.info.rank.canBuildFalse")));
            rankList += plugin.getLanguage().get("commands.kingdom.info.rank.item", rankVars);
        }

        first = true;

        for (KingdomMember member : kingdom.getMembers()) {
            if (first == false) {
                rankList += plugin.getLanguage().get("commands.kingdom.info.member.separator");
            }
            Map<String, Object> memberVars = new HashMap<>();
            memberVars.put("%N", member.getDwDPlayer().getName());
            memberVars.put("%R", member.getRank().getName());
            memberList += plugin.getLanguage().get("commands.kingdom.info.member.item", memberVars);

        }

        vars.put("%N", kingdom.getName());
        vars.put("%O", ((kingdom.getOwner() == null) ? "Nobody" : kingdom.getOwner()));
        vars.put("%W", kingdom.getWorld().getName());
        vars.put("%R", rankList);
        vars.put("%M", memberList);
        sender.sendMessage(plugin.getLanguage().get("commands.kingdom.info.render", vars));
    }

    @Command(aliases = {"delete", "d"}, usage = "", flags = "", desc = "Delete a kingdom", help = "[kingdom]", min = 1, max = 1)
    @CommandPermissions("dwdkingdoms.commands.kingdom.delete")
    @Console
    public static void delete(CommandContext args, CommandSender sender) throws CommandException {
        Kingdom kingdom = null;
        if (args.argsLength() == 1) {
            kingdom = plugin.getKingdomManager().getKingdom(args.getString(0));
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else if (sender instanceof Player) {
            Player player = (Player) sender;
            World world = player.getWorld();
            kingdom = plugin.getKingdomManager().getKingdom(world);
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%W", world.getName());
                sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldDoesntExist", vars));
                return;
            }
        } else {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%A", args.getString(0));
            sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
            return;
        }
        Map<String, Object> vars = new HashMap<>();
        vars.put("%N", kingdom.getName());
        vars.put("%W", kingdom.getWorld().getName());

        plugin.getKingdomManager().deleteKingdom(kingdom);
        sender.sendMessage(plugin.getLanguage().get("commands.kingdom.delete.deleted", vars));
    }

    @Command(aliases = {"name", "rename"}, usage = "", flags = "", desc = "Rename a Kingdom", help = "[Kingdom] <New Name>", min = 1, max = 2)
    @CommandPermissions("dwdkingdoms.commands.kingdom.name")
    @Console
    public static void name(CommandContext args, CommandSender sender) throws CommandException {
        Kingdom kingdom = null;
        String newName = "";
        if (args.argsLength() == 2) {
            kingdom = plugin.getKingdomManager().getKingdom(args.getString(0));
            newName = args.getString(1);
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else if (args.argsLength() == 1) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                World world = player.getWorld();
                kingdom = plugin.getKingdomManager().getKingdom(world);
                newName = args.getString(0);
                if (kingdom == null) {
                    Map<String, Object> vars = new HashMap<>();
                    vars.put("%W", world.getName());
                    sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldDoesntExist", vars));
                    return;
                }
            } else {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else {
            sender.sendMessage(plugin.getLanguage().get("commands.kingdom.name.help"));
            return;
        }

        Map<String, Object> vars = new HashMap<>();

        vars.put(
                "%ON", kingdom.getName());
        vars.put(
                "%W", kingdom.getWorld().getName());
        vars.put(
                "%NN", newName);

        kingdom.setName(newName);

        sender.sendMessage(plugin.getLanguage().get("commands.kingdom.name.renamed", vars));

    }

    @Command(aliases = {"owner", "o"}, usage = "", flags = "", desc = "Set the owner of a kingdom.", help = "[kingdom] <Player>", min = 1, max = 2)
    @CommandPermissions("dwdkingdoms.commands.kingdom.owner")
    @Console
    public static void owner(CommandContext args, CommandSender sender) throws CommandException {
        Kingdom kingdom = null;
        String playerName = "";
        if (args.argsLength() == 2) {
            kingdom = plugin.getKingdomManager().getKingdom(args.getString(0));
            playerName = args.getString(1);
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else if (args.argsLength() == 1) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                World world = player.getWorld();
                kingdom = plugin.getKingdomManager().getKingdom(world);
                playerName = args.getString(0);
                if (kingdom == null) {
                    Map<String, Object> vars = new HashMap<>();
                    vars.put("%W", world.getName());
                    sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldDoesntExist", vars));
                    return;
                }
            } else {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else {
            sender.sendMessage(plugin.getLanguage().get("commands.kingdom.owner.help"));
            return;
        }

        DwDPlayer dwdPlayer = new DwDPlayer(playerName);

        if (plugin.getKingdomManager().playerIsMember(dwdPlayer)) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%P", dwdPlayer.getName());
            sender.sendMessage(plugin.getLanguage().get("error.playerAlreadyMemberOfKingdom", vars));
            return;
        }

        kingdom.setOwner(dwdPlayer);
        Map<String, Object> vars = new HashMap<>();
        vars.put("%P", dwdPlayer.getName());
        vars.put("%K", kingdom.getName());
        sender.sendMessage(plugin.getLanguage().get("commands.kingdom.owner.set", vars));
    }
    
    @Command(aliases = {"coowner", "co"}, usage = "", flags = "", desc = "Set the co-owner of a kingdom.", help = "[kingdom] <Player>", min = 1, max = 2)
    @CommandPermissions("dwdkingdoms.commands.kingdom.coowner")
    @Console
    public static void coowner(CommandContext args, CommandSender sender) throws CommandException {
        Kingdom kingdom = null;
        String playerName = "";
        if (args.argsLength() == 2) {
            kingdom = plugin.getKingdomManager().getKingdom(args.getString(0));
            playerName = args.getString(1);
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else if (args.argsLength() == 1) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                World world = player.getWorld();
                kingdom = plugin.getKingdomManager().getKingdom(world);
                playerName = args.getString(0);
                if (kingdom == null) {
                    Map<String, Object> vars = new HashMap<>();
                    vars.put("%W", world.getName());
                    sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldDoesntExist", vars));
                    return;
                }
            } else {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else {
            sender.sendMessage(plugin.getLanguage().get("commands.kingdom.coowner.help"));
            return;
        }

        DwDPlayer dwdPlayer = new DwDPlayer(playerName);

        if (plugin.getKingdomManager().playerIsMember(dwdPlayer)) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%P", dwdPlayer.getName());
            sender.sendMessage(plugin.getLanguage().get("error.playerAlreadyMemberOfKingdom", vars));
            return;
        }

        kingdom.setCoOwner(dwdPlayer);
        Map<String, Object> vars = new HashMap<>();
        vars.put("%P", dwdPlayer.getName());
        vars.put("%K", kingdom.getName());
        sender.sendMessage(plugin.getLanguage().get("commands.kingdom.coowner.set", vars));
    }

    @Command(aliases = {"rank"}, usage = "", flags = "", desc = "Kingdom Rank commands.", help = "All commands related to Kingdom ranks.", min = 0, max = 0)
    @NestedCommand(value = {KingdomRankCommands.class})
    public static void rank(CommandContext args, CommandSender sender) throws CommandException {
    }
}
