/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms.commands;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import java.util.HashMap;
import java.util.Map;
import net.downwithdestruction.dwdcore.players.DwDPlayer;
import net.downwithdestruction.dwdkingdoms.DwDKingdomsPlugin;
import net.downwithdestruction.dwdkingdoms.objects.Kingdom;
import net.downwithdestruction.dwdkingdoms.objects.Plot;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Dan
 */
public class PlotCommands {

    private static DwDKingdomsPlugin plugin = DwDKingdomsPlugin.getPlugin();

    @Command(aliases = {"create", "c"}, usage = "", flags = "", desc = "Create a plot", help = "[kingdom] [plot number]", min = 0, max = 1)
    @CommandPermissions("dwdkingdoms.commands.plot.create")
    public static void create(CommandContext args, CommandSender sender) throws CommandException {
        Kingdom kingdom = null;
        int plotID = 0;
        if (args.argsLength() == 2) {
            kingdom = plugin.getKingdomManager().getKingdom(args.getString(0));
            plotID = args.getInteger(1);
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else if (sender instanceof Player) {
            Player player = (Player) sender;
            World world = player.getWorld();
            kingdom = plugin.getKingdomManager().getKingdom(world);
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%W", world.getName());
                sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldDoesntExist", vars));
                return;
            }

            if (args.argsLength() == 1) {
                plotID = args.getInteger(0);
            }

            if (plotID == 0) {
                int x = 1;
                Boolean found = false;
                while (found == false) {
                    if (!kingdom.hasPlot(x)) {
                        found = true;
                    }
                    x++;
                }
                plotID = x;
            }

            if (kingdom.getPlot(plotID) != null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%P", plotID + "");
                vars.put("%W", world.getName());
                vars.put("%N", kingdom.getName());
                sender.sendMessage(plugin.getLanguage().get("error.plotAlreadyExists", vars));
                return;
            }
        } else {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%A", args.getString(0));
            sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
            return;
        }

        WorldEditPlugin wePlugin = plugin.getWorldEdit();
        Player player = (Player) sender;
        Selection playerSelection = wePlugin.getSelection(player);

        Plot plot = new Plot(plotID, kingdom, playerSelection);
        kingdom.addPlot(plot);

        Map<String, Object> vars = new HashMap<>();
        vars.put("%K", kingdom.getName());
        vars.put("%I", plotID);
        sender.sendMessage(plugin.getLanguage().get("commands.plot.create.created", vars));
    }

    @Command(aliases = {"owner", "o"}, usage = "", flags = "", desc = "Set the owner of a plot", help = "[kingdom] <plot number> <player>", min = 2, max = 2)
    @CommandPermissions("dwdkingdoms.commands.plot.owner")
    @Console
    public static void owner(CommandContext args, CommandSender sender) throws CommandException {
        Kingdom kingdom = null;
        int plotID = 0;
        String playerName = "";

        if (args.argsLength() == 3) {
            kingdom = plugin.getKingdomManager().getKingdom(args.getString(0));
            plotID = args.getInteger(1);
            playerName = args.getString(2);
            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else if (sender instanceof Player) {
            Player player = (Player) sender;
            World world = player.getWorld();
            kingdom = plugin.getKingdomManager().getKingdom(world);

            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%W", world.getName());
                sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldDoesntExist", vars));
                return;
            }

            if (args.argsLength() == 2) {
                plotID = args.getInteger(0);
                playerName = args.getString(1);
            }

            if (kingdom.getPlot(plotID) == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%P", plotID + "");
                vars.put("%W", world.getName());
                vars.put("%N", kingdom.getName());
                sender.sendMessage(plugin.getLanguage().get("error.plotDoesntExist", vars));
                return;
            }
        } else {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%A", args.getString(0));
            sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
            return;
        }

        Plot plot = kingdom.getPlot(plotID);
        plot.setOwner(new DwDPlayer(playerName));


        Map<String, Object> vars = new HashMap<>();
        vars.put("%I", plotID + "");
        vars.put("%N", kingdom.getName());
        vars.put("%P", playerName);
        sender.sendMessage(plugin.getLanguage().get("commands.plot.owner.set", vars));
    }

    @Command(aliases = {"sell", "s"}, usage = "", flags = "", desc = "Sell a plot", help = "[kingdom] <plot number> <player>", min = 2, max = 3)
    @CommandPermissions("dwdkingdoms.commands.plot.sell")
    public static void sell(CommandContext args, CommandSender sender) throws CommandException {

        Kingdom kingdom = null;
        int plotID;
        String playerName;
        Player player = (Player) sender;

        if (args.argsLength() == 3) {
            kingdom = plugin.getKingdomManager().getKingdom(args.getString(0));
            plotID = args.getInteger(1);
            playerName = args.getString(2);

            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%A", args.getString(0));
                sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
                return;
            }
        } else {
            kingdom = plugin.getKingdomManager().getKingdom(player.getWorld());

            plotID = args.getInteger(0);
            playerName = args.getString(1);

            if (kingdom == null) {
                Map<String, Object> vars = new HashMap<>();
                vars.put("%W", player.getWorld().getName());
                sender.sendMessage(plugin.getLanguage().get("error.kingdomWorldDoesntExist", vars));
                return;
            }
        }

        if (!kingdom.hasPlot(plotID)) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%P", plotID + "");
            vars.put("%N", kingdom.getName());
            sender.sendMessage(plugin.getLanguage().get("error.plotDoesntExist", vars));
            return;
        }

        Plot plot = kingdom.getPlot(plotID);

        if (!plugin.getPlayerManager().playerOnline(playerName)) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%P", playerName);
            sender.sendMessage(plugin.getLanguage().get("error.playerNotOnline", vars));
            return;
        }

        if (plot.getOwner() != null) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%P", plotID + "");
            vars.put("%N", plot.getOwner());
            vars.put("%K", kingdom.getName());
            sender.sendMessage(plugin.getLanguage().get("error.plotAlreadyHasOwner", vars));
            return;
        }


        DwDPlayer targetPlayer = plugin.getPlayerManager().getPlayer(playerName);
        DwDPlayer sellerPlayer = plugin.getPlayerManager().getPlayer(player.getName());

        targetPlayer.getTempData().set("dwdkingdoms.sellRequest.inProgress", true);
        targetPlayer.getTempData().set("dwdkingdoms.sellRequest.plotID", plotID);
        targetPlayer.getTempData().set("dwdkingdoms.sellRequest.kingdom", kingdom.getName());

        Map<String, Object> vars = new HashMap<>();
        vars.put("%P", plotID + "");
        vars.put("%K", kingdom.getName());
        vars.put("%C", plot.getPrice() + "");
        vars.put("%S", sellerPlayer.getName());
        vars.put("%B", targetPlayer.getName());


        targetPlayer.getPlayer().sendMessage(plugin.getLanguage().get("commands.plot.sell.target", vars));
        sellerPlayer.getPlayer().sendMessage(plugin.getLanguage().get("commands.plot.sell.seller", vars));
    }

    @Command(aliases = {"accept", "yes", "y", "confirm"}, usage = "", flags = "", desc = "Confirm an action", help = "", min = 0, max = 0)
    @CommandPermissions("dwdkingdoms.commands.plot.accept")
    public static void accept(CommandContext args, CommandSender sender) throws CommandException {
        Player player = (Player) sender;
        DwDPlayer dwdPlayer = plugin.getPlayerManager().getPlayer(player.getName());

        if (dwdPlayer.getTempData().getBoolean("dwdkingdoms.sellRequest.inProgress", false) == false) {
            player.sendMessage(plugin.getLanguage().get("commands.plot.accept.noRequest"));
            return;
        }

        String kingdomName = dwdPlayer.getTempData().getString("dwdkingdoms.sellRequest.kingdom", "");
        Kingdom kingdom = plugin.getKingdomManager().getKingdom(kingdomName);

        if (kingdom == null) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%A", kingdomName);
            sender.sendMessage(plugin.getLanguage().get("error.kingdomDoesntExist", vars));
            return;
        }

        int plotID = dwdPlayer.getTempData().getInt("dwdkingdoms.sellRequest.plotID", 0);

        if (!kingdom.hasPlot(plotID)) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%P", plotID + "");
            vars.put("%N", kingdom.getName());
            sender.sendMessage(plugin.getLanguage().get("error.plotDoesntExist", vars));
            return;
        }

        Plot plot = kingdom.getPlot(plotID);

        if (plot.getOwner() != null) {
            Map<String, Object> vars = new HashMap<>();
            vars.put("%P", plotID + "");
            vars.put("%N", plot.getOwner());
            vars.put("%K", kingdom.getName());
            sender.sendMessage(plugin.getLanguage().get("error.plotAlreadyHasOwner", vars));
            return;
        }
        
        

    }

    @Command(aliases = {"decline", "no", "n", "reject"}, usage = "", flags = "", desc = "Decline an action", help = "", min = 0, max = 0)
    @CommandPermissions("dwdkingdoms.commands.plot.decline")
    public static void decline(CommandContext args, CommandSender sender) throws CommandException {
    }

    @Command(aliases = {"delete", "del", "d"}, usage = "", flags = "", desc = "Delete a plot", help = "<plot number>", min = 1, max = 1)
    @CommandPermissions("dwdkingdoms.commands.plot.delete")
    @Console
    public static void delete(CommandContext args, CommandSender sender) throws CommandException {
    }

    @Command(aliases = {"evict"}, usage = "", flags = "", desc = "Evict a plot owner", help = "<plot number>", min = 1, max = 1)
    @CommandPermissions("dwdkingdoms.commands.plot.evict")
    @Console
    public static void evict(CommandContext args, CommandSender sender) throws CommandException {
    }
}
