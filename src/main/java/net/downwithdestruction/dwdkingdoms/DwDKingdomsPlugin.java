/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdkingdoms;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import net.downwithdestruction.dwdcore.DwDPlugin;
import net.downwithdestruction.dwdkingdoms.commands.GeneralCommands;
import net.downwithdestruction.dwdkingdoms.objects.KingdomManager;
import org.bukkit.Bukkit;

/**
 *
 * @author Dan
 */
public class DwDKingdomsPlugin extends DwDPlugin {

    private static DwDKingdomsPlugin instance;
    private KingdomManager kingdomManager = new KingdomManager();
    private WorldEditPlugin wePlugin = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");

    @Override
    public void pluginEnable() {
        instance = this;
        registerCommand(GeneralCommands.class);
        kingdomManager.loadData();
    }

    @Override
    public void pluginDisable() {
        kingdomManager.saveData();
    }

    public static DwDKingdomsPlugin getPlugin() {
        return instance;
    }

    public KingdomManager getKingdomManager() {
        return kingdomManager;
    }

    public void runConsoleCommand(String command) {
        Bukkit.getServer().dispatchCommand(
                Bukkit.getServer().getConsoleSender(), command);
    }
    
    public WorldEditPlugin getWorldEdit() {
        return wePlugin;
    }
    
    public void addPermission(String player, String node, String world) {
        DwDKingdomsPlugin.getPlugin().runConsoleCommand("perms player setperm " + player
                    + " " + world + ":" + node + " true");
    }
    
    public void removePermission(String player, String node, String world) {
        DwDKingdomsPlugin.getPlugin().runConsoleCommand("perms player unsetperm " + player
                    + " " + world + ":" + node);
    }
}
